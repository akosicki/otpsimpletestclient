#!/bin/bash
set -e

ROUTER="default"
OTP_JAR=otp-1.1.0-SNAPSHOT-shaded.jar

run_instance() {
    local BASE_PATH="$1"
    local GRAPH_FILE="$BASE_PATH/graphs/$ROUTER/Graph.obj"
    local PORT="$2"
    local SECURE_PORT=$((PORT+1))

    if [ -f "$GRAPH_FILE" ];
    then
        java -Xmx2G -jar $OTP_JAR --basePath "$BASE_PATH" --router $ROUTER --verbose --port $PORT --securePort $SECURE_PORT &
    else
        echo "CAUTION: no $GRAPH_FILE file, it will take a few minutes to build it!" >&2
        java -Xmx2G -jar $OTP_JAR --build "$BASE_PATH/graphs/$ROUTER" --preFlight --verbose --port $PORT --securePort $SECURE_PORT &
    fi
}

run_instance "$PWD/otp/official" 8086
run_instance "$PWD/otp/prediction1" 8084
run_instance "$PWD/otp/prediction2" 8088
run_instance "$PWD/otp/prediction3" 8090
run_instance "$PWD/otp/real" 8082

trap 'kill $(jobs -p)' SIGINT SIGTERM EXIT
wait
