var rest = require('restler');
var querystring = require('querystring');
var async = require("async");
var geolib = require("geolib");
var fs = require('fs');
var ProgressBar = require('progress');

var argv = require('minimist')(process.argv.slice(2),{ "default" : {
	"time": "08:00am",
	"date" : "09-22-2016"
}});

simpleOutputFile = "trips_" + argv.time.replace(":","") + "_" + argv.date.replace("-","") + ".csv";
fullOutputFile= "trips_" + argv.time.replace(":","") + "_" + argv.date.replace("-","") + ".json";

var stopPairs = require('./randomStopPairs');

var OFFICIAL = { name: "official", port: 8086 };
var REAL = { name: "real", port: 8082 };
var PREDICTION1 = { name: "prediction1", port: 8084 };
var PREDICTION2 = { name: "prediction2", port: 8088 };
var PREDICTION3 = { name: "prediction3", port: 8090 };
var INSTANCES = [ OFFICIAL, REAL, PREDICTION1, PREDICTION2, PREDICTION3 ];

var results = {};

function queryUrl(port, begin, end) {
	// TODO use a single instance with a multiple routers
	var url = "http://localhost:" + port + "/otp/routers/default/plan?"
		+ querystring.stringify({
			fromPlace : begin.latitude + "," + begin.longitude,
			toPlace : end.latitude + "," + end.longitude,
			time : argv.time,
			date : argv.date,
			mode : "TRANSIT,WALK",
			maxWalkDistance : "1000",
			maxTransfers : 5,
			arriveBy : false,
			wheelchair : false,
			locale : "en"
		});

	return url;
};

var bar = new ProgressBar(' Querying OTP [:bar] :percent :etas', {
	complete: '=',
	incomplete: ' ',
	width: 80,
	total: stopPairs.length
});

function trip(pair, done) {
	bar.tick(1);

	var tripName = "from " + pair.begin.name + " to " + pair.end.name;
	results[tripName] = { "name" : tripName };

	var tasks = [];
	INSTANCES.forEach(function(i) {
		tasks.push(function (done) {
			rest.get(queryUrl(i.port, pair.begin, pair.end)).on('complete', function (data) {
				results[tripName][i.name] = data.plan;
				done();
			});
		});
	});

	async.parallel(tasks, function() {
		done();
	});
}

function getTime(plan) {
	if (plan) {
		startTime = plan.date;
		endTime = plan.itineraries[0].endTime;
		return Math.round((endTime - startTime) / 1000);
	} else {
		return -1;
	}
}

function finished() {
	console.log("\n Finished");

	var simpleOutput = "no, trip, official (sec), prediction1 (sec), prediction2 (sec), prediction3 (sec), real (sec)\n";
	var rowNumber = 1;
	Object.keys(results).forEach(function(key) {
		var trip = results[key];
		simpleOutput += rowNumber++ + ", " + trip.name + ", " +
			getTime(trip[OFFICIAL.name]) + ", " +
			getTime(trip[PREDICTION1.name]) + ", " +
			getTime(trip[PREDICTION2.name]) + ", " +
			getTime(trip[PREDICTION3.name]) + ", " +
			getTime(trip[REAL.name]) + '\n';
	});

	fs.writeFile(simpleOutputFile, simpleOutput, function(err) {
		if (err) return console.log(err);
		console.log(" " + simpleOutputFile + " saved succesfully");
	});

	fs.writeFile(fullOutputFile, JSON.stringify(results, null, 4), function(err) {
		if (err) return console.log(err);
		console.log(" " + fullOutputFile + " saved succesfully");
	});
}

console.log(" Computing transit times for " + stopPairs.length + " trips found in randomStopPairs.json\n");

async.eachSeries(stopPairs, trip, finished);
