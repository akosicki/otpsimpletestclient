#!/bin/bash
set -e

ROUTER="default"
DEBUG_PORT=5005
DEBUG_LINE="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=$DEBUG_PORT"

OFFICIAL_BASE="$PWD/otp/official"
OFFICIAL_GRAPH="$OFFICIAL_BASE/graphs/$ROUTER/Graph.obj"

OTP_JAR=otp-1.1.0-SNAPSHOT-shaded.jar 

echo running with socket debug server on localhost:5005

if [ -f "$OFFICIAL_GRAPH" ];
then
	java $DEBUG_LINE -Xmx2G -jar $OTP_JAR --basePath "$OFFICIAL_BASE" --router $ROUTER --verbose --port 8086 --securePort 8087 &
else
	echo "CAUTION: no $OFFICIAL_GRAPH file, it will take a few minutes to build it!" &
	sleep 2
	java $DEBUG_LINE -Xmx2G -jar $OTP_JAR --build "$OFFICIAL_BASE/graphs/$ROUTER" --preFlight --verbose --port 8086 --securePort 8087 &
fi

trap 'kill $(jobs -p)' SIGINT SIGTERM EXIT
wait
