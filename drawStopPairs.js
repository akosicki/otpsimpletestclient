var geolib = require("geolib");
var minimist = require('minimist');

var argv = minimist(process.argv.slice(2),{ "default" : { "pairs" : 100, minDistance: 5000 }});
var stops = require('./allStops.json').filter(function(stop) { return ! stop.isLoop; } );

function randomInt (bound) { return Math.floor(Math.random() * bound); }
function key(a, b) { return a + ":" + b; }

var used={}
var trips=[]

while (trips.length < argv.pairs) {
    var a,b, stop1, stop2, distance;
    do {
        a = randomInt(stops.length);
        b = randomInt(stops.length);
        stop1 = stops[a];
        stop2 = stops[b];
        distance = geolib.getDistance(stop1, stop2)
    } while(a == b || used[key(a,b)] || distance <= argv.minDistance)

    trips.push({ begin: stop1, end: stop2, distance: distance });
    used[key(a,b)] = true;
}

console.log( JSON.stringify(trips, null, 4));

