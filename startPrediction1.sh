#!/bin/bash
set -e

ROUTER="default"
DEBUG_PORT=5005
DEBUG_LINE="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=$DEBUG_PORT"

PREDICTION1_BASE="$PWD/otp/prediction1"
PREDICTION1_GRAPH="$PREDICTION1_BASE/graphs/$ROUTER/Graph.obj"

OTP_JAR=otp-1.1.0-SNAPSHOT-shaded.jar 

echo running with socket debug server on localhost:5005

if [ -f "$PREDITCION1_GRAPH" ];
then
	java $DEBUG_LINE -Xmx2G -jar $OTP_JAR --basePath "$PREDICTION1_BASE" --router $ROUTER --verbose --port 8084 --securePort 8085 &
else
	echo "CAUTION: no $PREDICTION1_GRAPH file, it will take a few minutes to build it!" &
	sleep 2
	java $DEBUG_LINE -Xmx2G -jar $OTP_JAR --build "$PREDICTION1_BASE/graphs/$ROUTER" --preFlight --verbose --port 8084 --securePort 8085 &
fi

trap 'kill $(jobs -p)' SIGINT SIGTERM EXIT
wait
