to run the script please

1. start the otp servers:

`$ ./startOTP.sh`

and wait till they are listening on their ports. Notice it might take several minutes to build the network graphs.

2. run the test client:

`$ ./runClient.sh`
